import os, sys, glob

commandsFilePath = sys.argv[1]
numPer = int(sys.argv[2])
maxChunks = int(sys.argv[3])
outDirPath = sys.argv[4]

with open(commandsFilePath) as commandsFile:
    commands = [line.rstrip("\n") for line in commandsFile]

if len(commands) == 0:
    print("There are no commands to execute!")
    sys.exit(1)

count = 0
while len(commands) > 0 and (maxChunks < 1 or count < maxChunks):
    count += 1
    outFilePath = "{}/{}".format(outDirPath, count)

    if len(commands) <= numPer:
        iterCommands = commands
        commands = []
    else:
        iterCommands = commands[:numPer]
        commands = commands[numPer:]

    with open(outFilePath, 'w') as outFile:
        outFile.write("\n".join(iterCommands) + "\n")
