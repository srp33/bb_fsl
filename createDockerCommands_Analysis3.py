import glob, gzip, os, shutil, sys

analysis = sys.argv[1]
startIteration = int(sys.argv[2])
stopIteration = int(sys.argv[3])
algorithmsFilePath = sys.argv[4]
classificationsFilePath = sys.argv[5]
outFileToCheck = sys.argv[6]
dockerOutFilePath = sys.argv[7]

currentWorkingDir = os.path.dirname(os.path.realpath(__file__))

dockerCommandFilePaths = []

with open(algorithmsFilePath, 'r') as f:
  allAlgorithms = f.read().splitlines()
allAlgorithms = [x.replace('AlgorithmScripts/Classification/', '') for x in allAlgorithms if not x.startswith("#")]
allAlgorithms = [x.split("__")[0] for x in allAlgorithms]
allAlgorithms = set(allAlgorithms)

with open(classificationsFilePath, 'r') as g:
  allClassifications = [x for x in g.read().splitlines() if not x.startswith("#")]

if os.path.exists(analysis + '_Commands/'):
  shutil.rmtree(analysis + '_Commands/')

for c in allClassifications:
  gseVar = c.split('\t')[0]
  classVar = c.split('\t')[1]
  covVar = c.split('\t')[2]

  input_data = list()
  dataset_path = 'Biomarker_Benchmark_Data/' + gseVar + '/'
  expression_path = dataset_path + gseVar + '.txt.gz'
  class_path = dataset_path + 'Class/' + classVar + '.txt'

  if covVar == 'no_covariates':
    continue
  else:
    input_data = covVar.split(',')
    input_data = [dataset_path + 'Covariate/' + i + '.txt' for i in input_data]

  input_data.append(expression_path)
  input_data.append(class_path)

  for i in range(startIteration, 1+stopIteration):
    print(analysis + ' ' + gseVar + ' ' + classVar + ' ' + 'iteration' + str(i))
    path = analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/*/' + outFileToCheck
    #print(path)
    #exit(0)

    executed_algos = glob.glob(path)
    executed_algos = [x.split('/')[4].replace('__','/',3) for x in executed_algos]
    executed_algos = set(executed_algos)

    not_executed_algos = allAlgorithms - executed_algos
    #not_executed_algos = [x for x in not_executed_algos if "extra_trees" in x][0:1]

    if len(not_executed_algos) > 0:
      for algo in not_executed_algos:
        algoName = algo.replace('/','__')

        data_all = ''
        for d in input_data:
          data_all = data_all + '--data "' + currentWorkingDir + '/' + d + '" '

        outDir = currentWorkingDir + '/' + analysis + '/' + gseVar + '/' + classVar + '/iteration' + str(i) + '/' + algoName + '/'

        out = 'module load python/3/6\nmodule load jdk/1.8.0-121\nmodule load zlib\nmodule load bzip2\nmodule load xz\nmodule load pcre\nexport PATH="$PATH:~/bb/Software/R-3.4.3/bin"\ncd ShinyLearner\n./UserScripts/classification_montecarlo ' + data_all + '--description ' + gseVar + '___' + classVar + '___iteration' + str(i) + ' --iterations 1 --classif-algo "AlgorithmScripts/Classification/' + algo + '*" --output-dir "' + outDir + '" --seed ' + str(i) + ' --verbose false\ncd "' + outDir + '"\nrm -f Log.txt\n'

        commandFilePath = analysis + '_Commands/{}/{}/iteration{}/{}.sh'.format(gseVar, classVar, i, algoName)
        if not os.path.exists(os.path.dirname(commandFilePath)):
          os.makedirs(os.path.dirname(commandFilePath))

        with open(commandFilePath, 'w') as outFile:
          outFile.write(out + '\n')

        dockerCommandFilePaths.append(commandFilePath)

if len(dockerCommandFilePaths) == 0:
    print('All commands have been executed!')
else:
    with open(dockerOutFilePath, 'w') as dockerOutFile:
        for command in dockerCommandFilePaths:
            dockerOutFile.write("bash {}\n".format(command))
